import "../styles/globals.css";
import type { AppProps } from "next/app";
import "@fontsource/montserrat/400.css";
import "@fontsource/montserrat/600.css";

function Proofcheck({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}

export default Proofcheck;
