import { useState, useRef, useEffect } from "react";
import type { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { Container, Grid } from "@mui/material";
import { spawn, Thread, Worker } from "threads";
import { Header } from "components/Header";
import { IPFSInput } from "components/IPFSInput";
import { Or } from "components/Or";
import { FileInput } from "components/FileInput";
import { VerifyButton } from "components/VerifyButton";
import { CheckResult } from "components/CheckResult";

const Home: NextPage = () => {
  const router = useRouter();
  const [file, setFile] = useState(null as any);
  const [responseJSON, setResponseJSON] = useState({});
  const [workerLoaded, setWorkerLoaded] = useState(false);
  const worker: any = useRef();
  const [statusMessage, setStatusMessage] = useState("");
  const [cid, setCID] = useState("");
  const [verifyNow, setVerifyNow] = useState(false);
  const [inProgress, setInProgress] = useState(false);
  const [completed, setCompleted] = useState(false);

  useEffect(() => {
    if (!worker.current) {
      const load = async () => {
        worker.current = await spawn(
          // @ts-expect-error
          new Worker(new URL("../workers/worker", import.meta.url))
        );
        worker.current.values().subscribe(({ type, message }: any) => {
          if (type === "complete") {
            const result = JSON.parse(message);
            setResponseJSON(result);
          } else {
            setStatusMessage(message);
          }
        });
        setWorkerLoaded(true);
      };
      load();
    }
    return () => worker.current && Thread.terminate(worker.current);
  }, [router.isReady]);

  // set the cid
  useEffect(() => {
    if (workerLoaded) {
      const { asPath } = router;
      const fullCID = asPath?.split("#")?.[1] ?? "";
      const shouldVerify = !!fullCID && fullCID !== "";
      setCID(fullCID);
      setVerifyNow(shouldVerify);
    }
  }, [workerLoaded]);

  // verify the cid
  useEffect(() => {
    if (verifyNow) {
      verify();
    }
  }, [verifyNow]);

  const fileReceived = (files: File[]) => {
    if (files.length > 0) {
      setFile(files[0]);
    }
  };

  const reset = (all = true) => {
    setVerifyNow(false);
    setResponseJSON({});
    setStatusMessage("");
    setInProgress(false);
    setCompleted(false);

    if (all) {
      setFile(null);
      setCID("");
    }
  };

  const verify = async () => {
    if (inProgress) return;

    reset(false);
    setInProgress(true);

    if (file) {
      await worker.current?.checkZip?.(file);
    } else if (cid && cid !== "") {
      await worker.current?.checkCID?.(cid);
    }

    setInProgress(false);
    setCompleted(true);
  };

  const formHidden = inProgress || completed;

  return (
    <>
      <Head>
        <title>Proofcheck for Proofmode</title>
      </Head>
      <Container maxWidth={completed ? "lg" : "sm"} sx={{ pt: 5, "*": { transition: "all 0.5s ease-in" } }}>
        <Grid container direction={completed ? "row" : "column"} spacing={2} justifyContent="space-between" alignItems="center">
          <Header completed={completed} />
          <IPFSInput cid={cid} setCid={setCID} hidden={formHidden} />
          <Or hidden={formHidden} />
          <FileInput
            file={file}
            fileReceived={fileReceived}
            hidden={formHidden}
          />
          <VerifyButton
            showProgress={inProgress}
            statusMessage={statusMessage}
            verify={verify}
            reset={reset}
            completed={completed}
            disabled={!file && cid === ""}
          />
        </Grid>
        <CheckResult responseJSON={responseJSON} />
      </Container>
    </>
  );
};

export default Home;
