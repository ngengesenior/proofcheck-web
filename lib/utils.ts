import crypto from "crypto";
import { loadPyodide, PyodideInterface } from "pyodide";
import * as zip from "@zip.js/zip.js";
import convert from "heic-convert";

export type RelatedFiles = {
    publicKey: string | null
    mediaFile: string | null
    mediaFileSignature: string | null
    proofCSV: string | null
    proofCSVSignature: string | null
    proofJSON: string | null
    proofJSONSignature: string | null
    ots: string | null
    gst: string | null
}

export const getRelatedFiles = async (fileName: string, allFiles: any): Promise<RelatedFiles> => {
    const fileHash = allFiles[fileName].hash;
    const fileNameOrNull = (fileName: string): string | null => {
        return allFiles[fileName] ? fileName : null;
    }

    return {
        publicKey: fileNameOrNull("pubkey.asc"),
        mediaFile: fileNameOrNull(fileName),
        mediaFileSignature: fileNameOrNull(`${fileHash}.asc`),
        proofCSV: fileNameOrNull(`${fileHash}.proof.csv`),
        proofCSVSignature: fileNameOrNull(`${fileHash}.proof.csv.asc`),
        proofJSON: fileNameOrNull(`${fileHash}.proof.json`),
        proofJSONSignature: fileNameOrNull(`${fileHash}.proof.json.asc`),
        ots: fileNameOrNull(`${fileHash}.ots`),
        gst: fileNameOrNull(`${fileHash}.gst`)
    }
};

const blobToBase64 = (blob: Blob): Promise<string | ArrayBuffer | null> => {
    return new Promise((resolve, _) => {
        const reader = new FileReader();
        reader.onloadend = () => resolve(reader.result);
        reader.readAsDataURL(blob);
    });
}

export const getThumbnail = async (fileName: string, allFiles: any): Promise<string | ArrayBuffer | null> => {
    const lowercaseFileName = fileName.toLowerCase();
    try {
        if (lowercaseFileName.endsWith(".heic") || lowercaseFileName.endsWith(".heif")) {
            const mediaFile = allFiles[fileName].blob;
            const arrayBuffer = await mediaFile.arrayBuffer();
            const jpeg = await convert({
                buffer: new Uint8Array(arrayBuffer),
                format: 'JPEG',
                quality: 1
            });
            const blob = new Blob([jpeg], { type: "image/jpeg" });
            const thumbnail = await blobToBase64(blob);

            return thumbnail;
        } else if (!lowercaseFileName.endsWith(".mp4") && !lowercaseFileName.endsWith(".mov")) {
            const mediaFile = allFiles[fileName].blob;
            const thumbnail = await blobToBase64(mediaFile);

            return thumbnail;
        } else {
            return null
        }
    } catch (error: any) {
        console.log(error);
        return null;
    }
};

export const hashFile = async (blob: Blob): Promise<string> => {
    const arrayBuffer = await blob.arrayBuffer();
    const hashSum = crypto.createHash('sha256');
    hashSum.update(new Uint8Array(arrayBuffer));
    const hash = hashSum.digest('hex');

    return hash;
}

export const unzipFile = async (zipFile: File, sendMessage: any): Promise<any> => {
    const entries = await new zip.ZipReader(new zip.BlobReader(zipFile)).getEntries();
    const files: any = {};
    for await (const entry of entries) {
        const { filename } = entry;

        if (filename.startsWith("__MACOSX/") || filename.endsWith("/")) continue;

        const endFilename = filename.split("/").pop();
        sendMessage({ type: "status", message: `Unzipping file ${endFilename}` });
        const blob = await entry.getData(new zip.BlobWriter());
        const hash = await hashFile(blob);
        files[endFilename!] = {
            hash,
            blob
        }
    }
    return files;
};

export const findMediaFiles = async (files: Record<string, any>): Promise<string[]> => {
    const mediaFileTypes = ["png", "jpg", ".jpeg", "aac", "mp3", "mp4", "mov", "wav", "ogg", "aif", "aiff", "heic", "heif"];
    // @ts-ignore
    const mediaFiles = Object.keys(files).filter((file: string) => mediaFileTypes.includes(file.split(".").pop().toLowerCase()));

    return mediaFiles;
};

export const createCombinedJSON = async (fileNames: string[], allFiles: Record<string, any>): Promise<string> => {
    const combinedJSON = await Promise.all(fileNames.map(async (fileName: string) => {
        const { proofJSON } = await getRelatedFiles(fileName, allFiles);
        // @ts-ignore
        const proofJSONString = await allFiles[proofJSON].blob.text();
        return JSON.parse(proofJSONString);
    }));

    return JSON.stringify(combinedJSON);
}

export const setupPyodide = async (proofcheckContext: any): Promise<PyodideInterface> => {
    const res = await fetch("/python/install.py");
    const install = await res.text();
    const pyodide = await loadPyodide({
        indexURL: "/pyodide/"
    });

    await pyodide.loadPackage(["micropip", "numpy", "matplotlib", "pandas", "geopandas"])
    await pyodide.runPythonAsync(install);
    pyodide.registerJsModule("proofcheck", proofcheckContext);

    return pyodide;
}
