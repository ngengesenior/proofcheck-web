import { fetchCID } from "./ipfs";
import { checkIntegrity } from "./integrity";
import { checkConsistency } from "./consistency";
import { checkSynchrony } from "./synchrony";
import { unzipFile, findMediaFiles, getRelatedFiles, setupPyodide, createCombinedJSON, getThumbnail } from "./utils";

type SendMessageOptions = {
    type: "status" | "complete",
    message: string
}
type SendMessage = (options: SendMessageOptions) => void;

const checkCommon = async (zipFile: File, sendMessage: SendMessage) => {
    sendMessage({
        type: "status", message: "Unzipping file..."
    });
    const allFiles = await unzipFile(zipFile, sendMessage);
    const mediaFiles = await findMediaFiles(allFiles);

    if (mediaFiles.length === 0) {
        sendMessage({
            type: "complete",
            message: JSON.stringify({
                summary: {
                    error: "No media files found"
                }
            })
        });
        return;
    }

    const final: any = {
        summary: {
            mediaSigned: 0,
            proofSigned: 0,
            opentimestampVerified: 0,
            safetyNetVerified: 0,
            c2paVerified: 0,
        }
    };
    const fileCount = mediaFiles.length;
    let mediaSignedCount = 0;
    let proofSignedCount = 0;
    let opentimestampVerifiedCount = 0;
    let safetyNetVerifiedCount = 0;
    let c2paVerifiedCount = 0;

    for (const fileName of mediaFiles) {
        sendMessage({ type: "status", message: `Checking integrity of file ${fileName}` });
        const fileHash = allFiles[fileName].hash;
        const relatedFiles = await getRelatedFiles(fileName, allFiles);
        const thumbnail = await getThumbnail(fileName, allFiles);
        const integrity = await checkIntegrity(allFiles, relatedFiles);
        final[fileHash] = {
            files: relatedFiles,
            thumbnail,
            integrity,
        };
        // @ts-expect-error
        mediaSignedCount += integrity?.pgp?.mediaFile?.verified ? 1 : 0;
        // @ts-expect-error
        proofSignedCount += integrity?.pgp?.proofJSON?.verified ? 1 : 0;
        // @ts-ignore
        opentimestampVerifiedCount += integrity?.ots?.verifyResult ? 1 : 0;
        safetyNetVerifiedCount += integrity?.safetyNet?.basicIntegrity ? 1 : 0;
        c2paVerifiedCount += integrity?.c2pa !== null ? 1 : 0;
    }

    final.summary.mediaSigned = mediaSignedCount / fileCount;
    final.summary.proofSigned = proofSignedCount / fileCount;
    final.summary.opentimestampVerified = opentimestampVerifiedCount / fileCount;
    final.summary.safetyNetVerified = safetyNetVerifiedCount / fileCount;
    final.summary.c2paVerified = c2paVerifiedCount / fileCount;

    if (final.summary.mediaSigned === 0 || final.summary.proofSigned === 0) {
        sendMessage({ type: "complete", message: JSON.stringify({ summary: { error: "No proof data found" } }) });
        return;
    }

    sendMessage({ type: "status", message: "Loading libraries..." });
    const proofJSON = await createCombinedJSON(mediaFiles, allFiles);
    const proofContext = { proofJSON }
    const pyodide = await setupPyodide(proofContext);
    sendMessage({ type: "status", message: "Checking consistency..." });
    final['consistency'] = await checkConsistency(pyodide, proofContext);

    sendMessage({ type: "status", message: "Checking synchrony..." });
    final['synchrony'] = await checkSynchrony(pyodide, proofContext);

    sendMessage({ type: "status", message: "Proof check complete" });

    // final.summary['cid'] = await addText(JSON.stringify(final));

    sendMessage({ type: "complete", message: JSON.stringify(final) });
}

export const checkCID = async (cid: string, sendMessage: SendMessage) => {
    sendMessage({ type: "status", message: `Fetching CID ${cid}` });
    const zipFile = await fetchCID(cid);
    await checkCommon(zipFile, sendMessage);
};

export const checkZip = async (zipFile: File, sendMessage: SendMessage) => {
    await checkCommon(zipFile, sendMessage);
};
