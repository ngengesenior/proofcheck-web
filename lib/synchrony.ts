export const checkSynchrony = async (pyodide: any, proofcheckContext: any) => {
    const res = await fetch("/python/synchrony.py");
    const synchrony = await res.text();
    try {
        const response = await pyodide.runPythonAsync(synchrony, proofcheckContext);
        const parsedResponse = JSON.parse(response);
        return parsedResponse;
    } catch (e: any) {
        return { error: e.toString() };
    }
};
