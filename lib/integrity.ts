import * as jose from "jose";
import * as openpgp from "openpgp";
import { default as initToolkit, getManifestStoreFromArrayBuffer } from '@contentauth/toolkit';
import * as OpenTimestamps from "opentimestamps";
import { RelatedFiles } from "./utils.js";
import mime from "mime-types";

const checkC2PA = async (fileName: string, fileBlob: Blob): Promise<any> => {
    try {
        await initToolkit();
        const mimeType = mime.lookup(fileName) || "application/octet-stream";
        const fileBuffer = await fileBlob.arrayBuffer();
        const res = await getManifestStoreFromArrayBuffer(fileBuffer, mimeType);

        return res;
    } catch (e: any) {
        return null;
    }
}

const checkOpenTimestamp = async (otsBlob: Blob, mediaFile: Blob) => {
    try {
        const file = await mediaFile.arrayBuffer();
        const detached = OpenTimestamps.DetachedTimestampFile.fromBytes(new OpenTimestamps.Ops.OpSHA256(), new Uint8Array(file));
        const fileOts = await otsBlob.arrayBuffer();
        const detachedOts = OpenTimestamps.DetachedTimestampFile.deserialize(new Uint8Array(fileOts));
        const options = {
            ignoreBitcoinNode: true,
            timeout: 5000
        }
        const verifyResult = await OpenTimestamps.verify(detachedOts, detached, options);
        const infoResult = OpenTimestamps.info(detachedOts);

        return { infoResult, verifyResult }
    } catch (e: any) {
        return { error: e.toString() }
    }
};

const checkPGP = async (publicKeyBlob: Blob, fileBlob: Blob, signatureBlob: Blob) => {
    const publicKeyString = await publicKeyBlob.text();
    const verificationKeys = await openpgp.readKey({ armoredKey: publicKeyString });
    const arrayBuffer = await fileBlob.arrayBuffer();
    const message = await openpgp.createMessage({ binary: new Uint8Array(arrayBuffer) });
    const signatureString = await signatureBlob.text();
    const signature = await openpgp.readSignature({
        armoredSignature: signatureString,
    })
    const verificationResult = await openpgp.verify({
        message,
        signature,
        verificationKeys
    });
    const { keyID, verified: v, signature: s } = verificationResult.signatures[0];
    const out: any = { key: keyID.toHex(), verified: false, createdAt: null };

    try {
        const verified = await v; // throws on invalid signature
        const sig = await s;
        out.verified = verified;
        out.createdAt = sig.packets[0].created;
    } catch (e) {
        console.log({ e })
    } finally {
        return out;
    }
};

const checkSafetyNet = async (jwtBlob: Blob) => {

    /*
    const certString = `-----BEGIN CERTIFICATE-----
      MIIFbzCCBFegAwIBAgIRAP4UU7By5wpREFFKwIqks44wDQYJKoZIhvcNAQELBQAw
      RjELMAkGA1UEBhMCVVMxIjAgBgNVBAoTGUdvb2dsZSBUcnVzdCBTZXJ2aWNlcyBM
      TEMxEzARBgNVBAMTCkdUUyBDQSAxRDQwHhcNMjIwNTE5MTIwMzI0WhcNMjIwODE3
      MTIwMzIzWjAdMRswGQYDVQQDExJhdHRlc3QuYW5kcm9pZC5jb20wggEiMA0GCSqG
      SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDgn3Xmw1zq9y3yrAZAxTs1ztTFoXaK7DbJ
      sejbWZx+wzkuYa9r4hg25JAGVq7z16CNtsW9XvNpc5HcLovn/oS/kiPsW+hOKhrP
      GPGBxK/OQHkKyHUcej50FtNBLOG5V7Vsw8VjxF5COVMCebvwwYJqx6ghGoKdk2cr
      ePS2hb+/Jp7SfPeZpZfyPYAbHVzS4W2ZLIj9CgsKOkvKhuXeVyv1+py9l3SWjsmi
      qQOvmEs+n+pqxAj+e1DbOUFjzX5Zf3rIYFubua861GwKSwcyVO4WfTd9QwzXYH9H
      VMkVWJaQs+nKEIN8//7vnLwBoDsV8c56GfwSgVxyDg5mwowRAwATAgMBAAGjggJ/
      MIICezAOBgNVHQ8BAf8EBAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUHAwEwDAYDVR0T
      AQH/BAIwADAdBgNVHQ4EFgQU4arkLvXyL3etEbDTwBjGX8qHMGUwHwYDVR0jBBgw
      FoAUJeIYDrJXkZQq5dRdhpCD3lOzuJIwewYIKwYBBQUHAQEEbzBtMDgGCCsGAQUF
      BzABhixodHRwOi8vb2NzcC5wa2kuZ29vZy9zL2d0czFkNGludC9vUldMQTJWOGxk
      YzAxBggrBgEFBQcwAoYlaHR0cDovL3BraS5nb29nL3JlcG8vY2VydHMvZ3RzMWQ0
      LmRlcjAdBgNVHREEFjAUghJhdHRlc3QuYW5kcm9pZC5jb20wIQYDVR0gBBowGDAI
      BgZngQwBAgEwDAYKKwYBBAHWeQIFAzA/BgNVHR8EODA2MDSgMqAwhi5odHRwOi8v
      Y3Jscy5wa2kuZ29vZy9ndHMxZDRpbnQvX0ZQcXFJSGdYNjguY3JsMIIBBAYKKwYB
      BAHWeQIEAgSB9QSB8gDwAHYARqVV63X6kSAwtaKJafTzfREsQXS+/Um4havy/HD+
      bUcAAAGA3Gp6vAAABAMARzBFAiBSj7BNIfmjbvndlwJ5yYE8OkX8SkzelQk5f4RD
      WdqxpwIhAPcQi72oNSOHl3lik27iNS+5rmWkY5w5WQFjOJuf5m2sAHYAQcjKsd8i
      RkoQxqE6CUKHXk4xixsD6+tLx2jwkGKWBvYAAAGA3Gp60gAABAMARzBFAiAlnC8C
      6ZzjgvTBWzNv5AsrAraE8U1lCb/m2Rgg5JgcDAIhAOB3kTEmnAucyr1xajEn57EP
      gcPg0DywXgkXM3d1UdXpMA0GCSqGSIb3DQEBCwUAA4IBAQB41MX1DI+a9AWPNcir
      C34LasF2hxldXXI5UD1fcnIlarnzmLsTurIF7m0j3ULvwU2+g5jy+xy6QCORuMFv
      KNPd2NVGUvOytj/nYr7Oa9tyAyRR79ZgOooEgRWVrWYzG2/JVXB3itVzBCZyClPA
      KXnSXQQsjOJ3HhiLjUHfaYxPYtCOwKGufxhVw/ptzlLB4HQgqIYvT8mJ84mZ8dhA
      9BJ6qQHPVuI3CuSR5TLdCICozDAaTsQg4g6H1X3WwA6scmbL/lAv4gInKS8TKwpJ
      y9XN3wv3ixtYeZpH3HvVYbtbbfLKJ5YONeoIvbCChGz9fk4GmPLf3kI7Sq3g/V7I
      rwO1
      -----END CERTIFICATE-----`;
    const cert = new X509Certificate(certString);

    const { payload, protectedHeader } = await jose.jwtVerify(jwt, cert.raw, {
      issuer: "urn:example:issuer",
      audience: "urn:example:audience",
    });
    console.log({ payload, protectedHeader });
    */
    const jwtString = await jwtBlob.text();
    try {
        const json = jose.decodeJwt(jwtString) //, null, "RS256");
        return json;
    } catch (e) {
        return { error: e };
    }
};

export const checkIntegrity = async (allFiles: Record<string, any>, relatedFiles: RelatedFiles) => {
    const {
        publicKey,
        mediaFile,
        mediaFileSignature,
        proofCSV,
        proofCSVSignature,
        proofJSON,
        proofJSONSignature,
        ots,
        gst
    } = relatedFiles;

    const mediaFileBlob = mediaFile ? allFiles[mediaFile].blob : null;

    let pgp = { mediaFile: null, proofCSV: null, proofJSON: null };

    if (publicKey) {
        const publicKeyBlob = allFiles[publicKey].blob;

        if (publicKeyBlob) {
            if (mediaFile && mediaFileSignature) {
                const mediaFileSignatureBlob = allFiles[mediaFileSignature].blob;
                pgp.mediaFile = await checkPGP(publicKeyBlob, mediaFileBlob, mediaFileSignatureBlob);
            }

            if (proofCSV && proofCSVSignature) {
                const proofCSVBlob = allFiles[proofCSV].blob;
                const proofCSVSignatureBlob = allFiles[proofCSVSignature].blob;
                pgp.proofCSV = await checkPGP(publicKeyBlob, proofCSVBlob, proofCSVSignatureBlob);
            }

            if (proofJSON && proofJSONSignature) {
                const proofJSONBlob = allFiles[proofJSON].blob;
                const proofJSONSignatureBlob = allFiles[proofJSONSignature].blob;
                pgp.proofJSON = await checkPGP(publicKeyBlob, proofJSONBlob, proofJSONSignatureBlob);
            }
        }
    }

    let safetyNet = null;
    if (gst) {
        const gstBlob = allFiles[gst].blob;
        if (gstBlob) {
            safetyNet = await checkSafetyNet(gstBlob);
        }
    }

    let otsResult = null;
    if (ots && mediaFile) {
        const otsBlob = allFiles[ots].blob;
        if (otsBlob && mediaFileBlob) {
            otsResult = await checkOpenTimestamp(otsBlob, mediaFileBlob);
        }
    }

    let c2pa = null;
    if (c2pa) {
        const c2paBlob = allFiles[c2pa].blob;
        if (c2paBlob && mediaFileBlob) {
            c2pa = await checkC2PA(c2paBlob, mediaFileBlob);
        }
    }

    return { pgp, safetyNet, ots: otsResult, c2pa }
};
