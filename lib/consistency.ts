export const checkConsistency = async (pyodide: any, proofcheckContext: any) => {
    const res = await fetch("/python/consistency.py");
    const consistency = await res.text();

    try {
        const response = await pyodide.runPythonAsync(consistency, proofcheckContext);
        const parsedResponse = JSON.parse(response);
        return parsedResponse;
    } catch (e: any) {
        return { error: e.toString() };
    }
};
