// import * as IPFS from "kubo-rpc-client";
// const url = new URL("https://test-api.gpfs.link");

export const fetchCID = async (cid: string): Promise<File> => {
    const res = await fetch(`https://test-gateway.gpfs.link/ipfs/${cid}`);
    const blob = await res.blob();
    const file = new File([blob as any], cid, {
        lastModified: new Date().getTime(),
        type: "application/zip"
    })

    return file;

    /*
    const ipfs = IPFS.create({ url });

    try {
        const fileData = [];
        for await (const chunk of ipfs.cat(cid)) {
            // @ts-ignore
            fileData.push(chunk);
        }

        const fileName = cid.split("/").pop();
        const filePath = `/tmp/${fileName}`;
        console.log({ fileData });

        return filePath;
    } catch (error) {
        console.log(error);
        return "";
    }
    */
};

export const addText = async (text: string): Promise<string> => {
    /*
    const ipfs = IPFS.create({ url });

    try {
        const { cid } = await ipfs.add(text);

        return cid.toString();
    } catch (error) {
        console.log(error);
        return "";
    }
*/
    return "";
};

