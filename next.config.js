/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: { unoptimized: true},
  webpack: (config) => {
    config.experiments = {
      ...config.experiments,
      asyncWebAssembly: true,
      layers: true,
    };
    config.module.rules.push({
      test: /\.py$/,
      use: "raw-loader",
    });
    config.module.rules.push({
      resourceQuery: /file/,
      type: "asset/resource",
    });
    config.resolve.fallback = {
      fs: false,
      tls: false,
      net: false,
      path: false,
      zlib: false,
      http: false,
      https: false,
      stream: false,
      crypto: false,
    };
    return config;
  },
};

module.exports = nextConfig;
