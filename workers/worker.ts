import { checkZip, checkCID } from "lib/check";
import { Observable, Subject } from "threads/observable"
import { expose } from "threads/worker"

type SendMessageOptions = {
    type: "status" | "complete",
    message: string
}
type SendMessage = (options: SendMessageOptions) => void;

let subject = new Subject();

const sendMessage: SendMessage = ({ type, message }: SendMessageOptions) => {
    subject.next({ type, message })
}

const check = {
    checkZip: (data: File) => {
        return checkZip(data, sendMessage);
    },
    checkCID: (cid: string) => {
        return checkCID(cid, sendMessage)
    },
    values: () => {
        return Observable.from(subject);
    }
}

expose(check);





