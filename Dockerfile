FROM node:18 as builder
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
ARG APP_DIR=/opt/proofcheck/
RUN mkdir -p ${APP_DIR}
COPY . ${APP_DIR}
RUN chown -R node ${APP_DIR}
USER node
WORKDIR ${APP_DIR}
RUN npm install --force
RUN npm run build

FROM node:18
ARG BUILD_DATE
ARG VERSION
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.version=$VERSION
ARG APP_DIR=/opt/proofcheck/
ENV APP_DIR ${APP_DIR}

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends \
    dumb-init
RUN mkdir -p ${APP_DIR}
RUN chown -R node ${APP_DIR}
USER node
WORKDIR ${APP_DIR}
COPY --from=builder ${APP_DIR}node_modules ./node_modules
COPY --from=builder ${APP_DIR}.next ./.next
COPY --from=builder ${APP_DIR}next.config.js ./next.config.js
COPY --from=builder ${APP_DIR}package.json ./
EXPOSE 3000
ENV PORT 3000
ENV NODE_ENV production
ENTRYPOINT ["dumb-init", "--"]
CMD ["npm", "run", "start"]
