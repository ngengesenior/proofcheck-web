import { FC } from "react";
import { Grid, Box, Typography, Button } from "@mui/material";

const fontProps = { fontFamily: "Montserrat, sans-serif" };

type OrProps = {
  hidden: boolean;
};

export const Or: FC<OrProps> = ({ hidden }) => (
  <Grid
    item
    container
    direction="column"
    spacing={2}
    sx={{ display: hidden ? "none" : "inherit" }}
  >
    <Grid
      item
      container
      direction="row"
      flexWrap="nowrap"
      spacing={2}
      alignItems="center"
    >
      <Grid item flexGrow={1}>
        <Box sx={{ height: "1px", width: "100%", backgroundColor: "black" }} />
      </Grid>
      <Grid item>
        <Typography
          variant="body2"
          sx={{
            textAlign: "center",
            textTransform: "uppercase",
            fontWeight: "bold",
            ...fontProps,
          }}
        >
          or
        </Typography>
      </Grid>
      <Grid item flexGrow={1}>
        <Box sx={{ height: "1px", width: "100%", backgroundColor: "black" }} />
      </Grid>
    </Grid>
  </Grid>
);
