import { FC, useState } from "react";
import { Grid, Button, Collapse } from "@mui/material";
import { JSONViewer } from "./JSONViewer";
import { VegaVisualization } from "./VegaVisualization";

const fontProps = { fontFamily: "Montserrat, sans-serif" };
const green = "#00a878";

type SuccessDetailsProps = {
  responseJSON: Record<string, any>;
};

export const SuccessDetails: FC<SuccessDetailsProps> = ({ responseJSON }) => {
  const [showDetails, setShowDetails] = useState(false);
  const filteredResponseJSON: any = Object.keys(responseJSON)
    .filter((key) => !["summary", "consistency", "synchrony"].includes(key))
    .reduce((acc, key) => {
      return {
        ...acc,
        [key]: responseJSON[key],
      };
    }, {});

  return (
    <>
      <Grid item>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "white",
            color: green,
            fontWeight: "bold",
            mt: 6,
            mb: 3,
            borderRadius: 500,
            ...fontProps,
          }}
          onClick={() => setShowDetails(true)}
        >
          Show Details
        </Button>
      </Grid>
      <Grid item>
        <Collapse in={showDetails}>
          <Grid container direction="column">
            {responseJSON.synchrony?.centroid_map_html && (
              <iframe
                height={600}
                srcDoc={responseJSON.synchrony?.centroid_map_html}
              />
            )}
            {Object.keys(filteredResponseJSON).map((key: string) => (
              <img
                key={key}
                src={filteredResponseJSON[key]?.thumbnail}
                width="500"
              />
            ))}
            <VegaVisualization responseJSON={responseJSON} />
        
          </Grid>
        </Collapse>
      </Grid>
    </>
  );
};
