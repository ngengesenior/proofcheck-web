import { FC } from "react";
import { Grid, Box } from "@mui/material";
import { VegaLite } from "react-vega";

const border = "2px solid #eee";

type VegaVisualizationProps = {
  responseJSON: Record<string, any>;
};

export const VegaVisualization: FC<VegaVisualizationProps> = ({
  responseJSON,
}) => {
  const rawVisualization = responseJSON.consistency?.chart ?? "{}";
  const visualization = JSON.parse(rawVisualization);

  return (
    <Grid item>
      <Box sx={{ borderTop: border, mt: 3, px: 1, py: 3 }}>
        <Box
          sx={{
            backgroundColor: "white",
            textAlign: "center",
          }}
        >
          <VegaLite spec={visualization} />
        </Box>
      </Box>
    </Grid>
  );
};
