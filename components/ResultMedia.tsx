import { FC } from "react";
import { Grid, Box } from "@mui/material";
import { FailureResult } from "./FailureResult";

type ResultMediaProps = {
    responseJSON: Record<string, any>;
};

export const ResultMedia: FC<ResultMediaProps> = ({ responseJSON }) => {
    const filteredResponseJSON: any = Object.keys(responseJSON)
        .filter((key) => !["summary", "consistency", "synchrony"].includes(key))
        .reduce((acc, key) => {
            return {
                ...acc,
                [key]: responseJSON[key],
            };
        }, {});

    return (
        <Box sx={{ width: "100%", height: "100%" }}>
            <Grid
                container
                direction="column"
                alignContent="center"
                spacing={2}
            >
                {
                    Object.keys(filteredResponseJSON).map((key: string) => (
                        <Grid item key={key}>
                            <img
                                key={key}
                                src={filteredResponseJSON[key]?.thumbnail}
                                style={{ width: "100%" }}
                            />
                        </Grid>
                    ))
                }
                <FailureResult responseJSON={responseJSON} />
            </Grid >
        </Box>
    );
}
