import { FC } from "react";
import { Grid, Box } from "@mui/material";
import { Checkboxes } from "./Checkboxes";
import { Map } from "./Map";
// import { VegaVisualization } from "./VegaVisualization";

type ResultSidebarProps = {
  responseJSON: Record<string, any>;
};

export const ResultSidebar: FC<ResultSidebarProps> = ({ responseJSON }) => (
  <Box sx={{ width: "100%", height: "100%" }}>
    <Grid
      item
      container
      direction="column"
      spacing={0}
    >
      <Checkboxes responseJSON={responseJSON} />
      <Map responseJSON={responseJSON} />
      { /* <VegaVisualization responseJSON={responseJSON} /> */}
    </Grid>
  </Box>
);
