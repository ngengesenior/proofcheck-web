import { FC } from "react";
import { Box, Grid, Typography } from "@mui/material";
import dynamic from "next/dynamic";

const DynamicReactJson: any = dynamic(import("react-json-view"), {
  ssr: false,
});

type JSONViewerProps = {
  responseJSON: Record<string, any>;
};

export const JSONViewer: FC<JSONViewerProps> = ({ responseJSON }) => (
  <Grid item container direction="column">
    <Grid item container direction="row">
      <Typography variant="h5" sx={{
        fontFamily: "Montserrat, sans-serif",
        fontWeight: 700,
        color: "#00a878",
        ml: 2,
        mt: 2
      }}>Proofcheck Response</Typography>
    </Grid>
    <Grid item>
      {Object.keys(responseJSON).length > 0 && (
        <Box
          sx={{
            margin: "0 auto",
            mt: 0,
            border: "1px solid white",
            overflow: "hidden",
          }}
        >

          <DynamicReactJson
            src={responseJSON}
            blindbbb
            collapseStringsAfterLength={50}
            collapsed={3}
            theme="bright:inverted"
            style={{ padding: "20px", width: "100%" }}
          />
        </Box>
      )}
    </Grid>
  </Grid>
);
