import { FC } from "react";
import Image from "next/image";
import { Grid, Typography } from "@mui/material";
import proveIt from "public/images/proveit.svg";

const fontProps = { fontFamily: "Montserrat, sans-serif" };

type HeaderProps = {
  completed: boolean;
};

export const Header: FC<HeaderProps> = ({ completed }) => (
  <Grid container item xs={completed ? 4 : 12} direction={completed ? "row" : "column"} spacing={2} alignItems="center">
    <Grid item sx={{ textAlign: "center" }}>
      <Image src={proveIt} alt="I can prove it" priority width={completed ? 70 : 250} />
    </Grid>
    <Grid item>
      <Typography
        variant={completed ? "h5" : "h2"}
        sx={{ fontWeight: 700, textAlign: completed ? "left" : "center", mt: completed ? 0 : 3, ...fontProps }}
      >
        Proofcheck
      </Typography>
      <Typography
        variant="h6"
        sx={{
          textTransform: "uppercase",
          textAlign: completed ? "left" : "center",
          fontWeight: "bold",
          fontSize: completed ? 10 : 16,
          mb: completed ? 0 : 3,
          ...fontProps,
        }}
      >
        Verification by Proofmode
      </Typography>
    </Grid>
  </Grid>
);
