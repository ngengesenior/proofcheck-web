import { FC } from "react";
import { Grid, Typography } from "@mui/material";

const fontProps = { fontFamily: "Montserrat, sans-serif" };

type FailureResultProps = {
  responseJSON: Record<string, any>;
};

export const FailureResult: FC<FailureResultProps> = ({ responseJSON }) => {
  const error = responseJSON?.summary?.error ?? null;

  return error ? (
    <Grid
      item
      sx={{
        textAlign: "center",
      }}
    >
      <Typography
        variant="body1"
        sx={{
          color: "#dd1111", pt: 4, pb: 8, ...fontProps
        }}
      >
        {error}
      </Typography >
    </Grid >
  ) : (
    <Grid item />
  );
}
