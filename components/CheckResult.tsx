import { FC } from "react";
import { Grid, Box, } from "@mui/material";
import { ResultMedia } from "./ResultMedia"
import { ResultSidebar } from "./ResultSidebar";

const border = "2px solid #eee";

type CheckResultProps = {
  responseJSON: Record<string, any>;
};

export const CheckResult: FC<CheckResultProps> = ({ responseJSON }) => {
  const success = Object.keys(responseJSON).length > 0 && !responseJSON["error"];

  return (
    <Box sx={{ width: "100%", height: "100%", borderTop: border, mt: 2, opacity: success ? 1 : 0 }}>
      <Grid container direction="row-reverse">
        <Grid item xs={12} sm={4}>
          <Box sx={{ height: "100%", borderLeft: border, p: 4 }}>
            <ResultSidebar responseJSON={responseJSON} />
          </Box>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Box sx={{ height: "100%", p: 4 }}>
            <ResultMedia responseJSON={responseJSON} />
          </Box>
        </Grid>
      </Grid >
    </Box>)
}
