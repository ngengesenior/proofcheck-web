import { FC, useState } from "react";
import { Checkbox, Grid, Box, Dialog, Button } from "@mui/material";
import { JSONViewer } from "./JSONViewer";

const green = "#00a878";

const CheckboxItem: FC<{ checked: boolean; indeterminate: boolean; label: string }> = ({ checked, indeterminate, label }) => (
  <Grid item xs={12}>
    <Box sx={{ backgroundColor: "#eee", width: "100%", p: 2, borderRadius: 2 }}>
      <Grid container direction="row" alignItems="center">
        <Grid item xs={10}>
          {label}
        </Grid>
        <Grid item xs={2}>
          <Checkbox size="medium" checked={checked} indeterminate={indeterminate} disabled sx={{
            "& .MuiSvgIcon-root": {
              color: green,
            }
          }} />
        </Grid>
      </Grid>
    </Box>
  </Grid>
);

type CheckboxesProps = {
  responseJSON: Record<string, any>;
};

export const Checkboxes: FC<CheckboxesProps> = ({ responseJSON }) => {
  const [dialogOpen, setDialogOpen] = useState(false);
  const filteredResponseJSON: any = Object.keys(responseJSON)
    .filter((key) => !["summary", "consistency", "synchrony"].includes(key))
    .reduce((acc, key) => {
      return {
        ...acc,
        [key]: responseJSON[key],
      };
    }, {});
  const mediaSigned = responseJSON.summary?.mediaSigned ?? 0;
  const proofSigned = responseJSON.summary?.proofSigned ?? 0;
  const opentimestampVerified =
    responseJSON.summary?.opentimestampVerified ?? 0;
  const safetyNetVerified = responseJSON.summary?.safetyNetVerified ?? 0;
  const c2paVerified = responseJSON.summary?.c2paVerified ?? 0;

  return (
    <Grid item container spacing={2}>
      <Grid item>
        <Box sx={{ fontWeight: "bold" }}>{`${Object.keys(filteredResponseJSON).length} file(s)`}</Box>
      </Grid>
      <CheckboxItem indeterminate={mediaSigned > 0 && mediaSigned < 1} checked={mediaSigned === 1} label="Media signed" />
      <CheckboxItem indeterminate={proofSigned > 0 && proofSigned < 1} checked={proofSigned === 1} label="Proof data present" />
      <CheckboxItem indeterminate={opentimestampVerified > 0 && opentimestampVerified < 1} checked={opentimestampVerified === 1} label="OpenTimestamp verified" />
      <CheckboxItem indeterminate={safetyNetVerified > 0 && safetyNetVerified < 1} checked={safetyNetVerified === 1} label="SafetyNet verified" />
      <CheckboxItem indeterminate={c2paVerified > 0 && c2paVerified < 1} checked={c2paVerified === 1} label="C2PA verified" />
      <Button sx={{ pt: 2, textTransform: "none", color: green, textAlign: "center", margin: "0 auto" }} onClick={() => setDialogOpen(true)}>View Report JSON</Button>

      <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)} maxWidth="md" >
        <JSONViewer responseJSON={responseJSON} />
      </Dialog>
    </Grid>
  );
};

