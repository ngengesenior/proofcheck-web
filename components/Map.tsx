import { FC } from "react";
import { Grid, Box } from "@mui/material";

const border = "2px solid #eee";

type MapProps = {
    responseJSON: Record<string, any>;
};

export const Map: FC<MapProps> = ({ responseJSON }) => (
    <Grid
        item
        container
        direction="column"
        spacing={2}
    >
        <Grid item>
            <Box sx={{ borderTop: border, mt: 3, px: 1, py: 3 }}>
                {responseJSON.synchrony?.centroid_map_html && (
                    <iframe
                        height={300}
                        srcDoc={responseJSON.synchrony?.centroid_map_html}
                        style={{ width: "100%", border: 0 }}
                    />
                )}
            </Box>
        </Grid>
    </Grid>
);
