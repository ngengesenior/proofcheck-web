import { FC } from "react";
import { Grid, CircularProgress, Button, Box } from "@mui/material";

const fontProps = { fontFamily: "Montserrat, sans-serif" };
const green = "#00a878";
const darkGreen = "#008946";

type VerifyButtonProps = {
  showProgress: boolean;
  statusMessage: string;
  verify: () => Promise<void>;
  reset: () => void;
  completed: boolean;
  disabled: boolean;
};

export const VerifyButton: FC<VerifyButtonProps> = ({
  showProgress,
  statusMessage,
  verify,
  reset,
  completed,
  disabled
}) => (
  <>
    {showProgress ? (
      <Grid item sx={{ textAlign: "center" }} xs={completed ? 3 : 12}>
        <Box
          sx={{
            mt: 3,
            mb: 3,
            color: green,
            fontWeight: "bold",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
            maxWidth: 550,
            ...fontProps,
          }}
        >
          {statusMessage}
        </Box>
        <CircularProgress size={60} thickness={6} sx={{ color: green }} />
      </Grid>
    ) : (
      <Grid item sx={{ textAlign: "center" }}>
        <Button
          variant="contained"
          disabled={disabled}
          sx={{
            backgroundColor: green,
            fontWeight: "bold",
            fontSize: completed ? 14 : 20,
            minWidth: 200,
            height: completed ? 40 : 70,
            px: 4,
            mt: showProgress || completed ? 0 : 6,
            mb: completed ? 0 : 4,
            borderRadius: 500,
            ...fontProps,
            "&:hover": {
              backgroundColor: darkGreen,
            },
          }}
          onClick={async (e) => {
            if (!completed && verify) {
              await verify();
            }
            if (completed && reset) {
              reset();
            }
          }}
        >
          {completed ? "Verify another file" : "Verify"}
        </Button>
      </Grid>
    )}
  </>
);
