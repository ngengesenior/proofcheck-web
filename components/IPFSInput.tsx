import { FC } from "react";
import { Grid, Typography, TextField } from "@mui/material";

const fontProps = { fontFamily: "Montserrat, sans-serif" };

type IPFSInputProps = {
  cid: string;
  setCid: (cid: string) => void;
  hidden?: boolean;
};

export const IPFSInput: FC<IPFSInputProps> = ({
  cid,
  setCid,
  hidden = false,
}) => (
  <Grid
    item
    container
    direction="column"
    spacing={2}
    sx={{ display: hidden ? "none" : "inherit" }}
  >
    <Grid item>
      <Typography
        variant="body1"
        sx={{ textAlign: "center", fontWeight: "bold", ...fontProps }}
      >
        Enter IPFS content ID
      </Typography>
    </Grid>
    <Grid item container direction="row" justifyContent="center" spacing={2}>
      <Grid item flexGrow={1}>
        <TextField
          value={cid}
          size="small"
          placeholder="bafybeigdyrzt5sfp7udm7hu76uh7y26nf3efuylqabf3oclgtqy55fbzdi"
          onChange={(e) => setCid(e.target.value)}
          sx={{
            width: "100%",
            "& fieldset": {
              borderRadius: 500,
            },
            input: {
              color: "#666",
              background: "#eee",
              borderRadius: 500,
              ...fontProps,
            },
          }}
        />
      </Grid>
    </Grid>
  </Grid>
);
