import { FC } from "react";
import { Grid, Box, Typography, Button } from "@mui/material";

const fontProps = { fontFamily: "Montserrat, sans-serif" };

type DownloadButtonProps = {
    hidden: boolean;
};

export const DownloadButton: FC<DownloadButtonProps> = ({ hidden }) => (<Box />
)

{/* <Box sx={{ textAlign: "center", fontWeight: "bold", color: "white" }}>
            Download this report here:
            <Box>
              <Link
                href={`https://test-gateway.gpfs.link/ipfs/${responseJSON.summary.cid}`}
              >
                <Box sx={{ color: "white", textDecoration: "underline" }}>
                  {`https://test-gateway.gpfs.link/ipfs/${responseJSON.summary.cid}`}
                </Box>
              </Link>
            </Box>
    </Box> */}
